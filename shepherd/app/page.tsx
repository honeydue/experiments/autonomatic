"use client"

import React, { useState, useEffect, useRef } from "react";
import Markdown from 'react-markdown'

export default function Home() {
    const [data, setData] = useState("");
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const inputRef = useRef(null);
    const [textareaHeight, setTextareaHeight] = useState('auto');

    const handleSubmit = async () => {
        setIsLoading(true);
        try {

            const inputValue = inputRef.current.value;
            console.log(JSON.stringify({prompt: inputValue}));
            const response = await fetch('http://localhost:8000/api/codegemma', {
                method: 'POST',
                body: JSON.stringify({
                    prompt: inputValue
                })
            });
            console.log("calling ollama!");
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const reader = response.body.getReader();
            const decoder = new TextDecoder();
            let result = '';

            const readStream = async () => {
                const { done, value } = await reader.read();
                if (done) {
                    return;
                }

                result += decoder.decode(value, { stream: true });

                try {
                    result = result.replaceAll("data: ", '');
                    console.log(result);
                    setIsLoading(false);
                    setData(result);
                    // result = '';  // Clear result after parsing
                } catch (e) {
                    console.log(e);
                    // Keep accumulating chunks until we have a valid JSON
                }
                await readStream();
            };
            await readStream();
        } catch (err) {
            setError(error.message);
        }
    }

    const handleInput = (event) => {
        setTextareaHeight('auto'); // Reset height to auto to shrink if necessary
        setTextareaHeight(`${inputRef.current.scrollHeight}px`); // Set to the scrollHeight to expand
    };

    useEffect(() => {
        setTextareaHeight(`${inputRef.current.scrollHeight}px`);
    }, []);

    const clearData = async () => {
            setData("");
    }

  return (
      <main className="flex flex-col items-center justify-between p-24">

          <div className="relative w-[1000px] mb-5 flex p-2 ps-5 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
              <textarea id="prompt" ref={inputRef}
                        style={{ height: textareaHeight }}
                        onInput={handleInput}
                        rows={1}
                     className="auto-resize block border-none rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                     placeholder="Prompt" required/>
              <button onClick={() => handleSubmit()}
                  type="submit"
                      className="ml-2 text-white max-h-10 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit
              </button>
          </div>
          <div>
              <div
                  className="w-[1000px] p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                  <a href="#">
                      <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Ollama FAQ</h5>
                  </a>
                  <div className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                      {isLoading ? 'Loading...' : <Markdown>{data}</Markdown>}
                  </div>
              </div>
          </div>

      </main>
  );
}
