from langchain_community.llms.ollama import Ollama
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, Request, HTTPException
from fastapi.responses import StreamingResponse

app = FastAPI()

origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


async def event_generator(prompt, model):
    llm = (Ollama(model=model).astream([prompt]))
    sentence = ""
    async for value in llm:
        if value.endswith("\n"):
            sentence += value.rstrip("\n")
            yield f"data: {sentence}\n\n"
            sentence = ""
        else:
            sentence += value

@app.get("/api")
async def root():
    return {"message": "Hello World"}

@app.get("/api/ollama/models")
async def ollama_models():
    models = Ollama().models
    return {"models": ["one", "two", "three"]}


@app.get("/api/ollama")
async def ollama():
    prompt = "What is the meaning of life?"
    return StreamingResponse(event_generator(prompt, "gemma"), media_type="text/event-stream")


@app.post("/api/gemma")
async def ollama(request: Request):
    body = await request.json()
    prompt = body.get("prompt", "")
    return StreamingResponse(event_generator(prompt, "gemma"), media_type="text/event-stream")

@app.post("/api/codegemma")
async def ollama(request: Request):
    body = await request.json()
    prompt = body.get("prompt", "")
    return StreamingResponse(event_generator(prompt, "codegemma"), media_type="text/event-stream")


@app.get("/api/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}


@app.get("/api/python")
def hello_world():
    return {"message": "Hello World"}
